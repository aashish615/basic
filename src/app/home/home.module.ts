import { AppRoutingModule } from './../app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { CustomDirectiveModule } from '../custom-directive/custom-directive.module';
import { CustomPipeModule } from '../custom-pipe/custom-pipe.module';

const routes :Routes= [
  {
    path:'',component:HomeComponent
  }
]

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CustomDirectiveModule,
    CustomPipeModule
  ],
  exports:[
    HomeComponent
  ]
})
export class HomeModule { }
