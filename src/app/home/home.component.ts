import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data:any=[
    {
      name:"Ashish", gender:'male',

    },
    {
      name:'usha', gender:'famale'
    }
  ];

  event_sponsors:any= [
    {
      "id": 59,
      "uuid": "60da7321-5b88-49b2-a62a-9b92b7127d6c",
      "user_id": null,
      "event_id": 963,
      "sponsor_name": "Sponsor 1",
      "representative_name": "John",
      "representative_email": "john@mailinator.com",
      "about_sponsor": "lasdjflaskdjfalksdjf",
      "sponsorship_type": "sponser",
      "sponsorship_type_other": null,
      "sponsor_level": "silver",
      "app_visibility_level": 1,
      "created_at": "2020-12-27T18:57:38.000000Z",
      "created_by": 171,
      "updated_at": "2020-12-27T18:57:38.000000Z",
      "sponsor_logo": {
        "id": 84,
        "uuid": "d4a175d8-1279-4483-b563-e844fbaefa7f",
        "sponsor_id": 59,
        "sponsor_logo": "images\/SponsorLogo\/5fe8d9229adc6_sponsor_logo_.png",
        "created_at": "2020-12-27T18:57:38.000000Z",
        "updated_at": "2020-12-27T18:57:38.000000Z"
      }
    },
    {
      "id": 60,
      "uuid": "45e58221-92cc-4a82-ae03-1f2e2eef6c9f",
      "user_id": null,
      "event_id": 963,
      "sponsor_name": "Sponsor 2-gold",
      "representative_name": "Doe",
      "representative_email": "doe@mailinator.com",
      "about_sponsor": "j lasjdf aksjdflaksjdf",
      "sponsorship_type": "sponser",
      "sponsorship_type_other": null,
      "sponsor_level": "gold",
      "app_visibility_level": 0,
      "created_at": "2020-12-27T18:58:17.000000Z",
      "created_by": 171,
      "updated_at": "2020-12-27T18:58:17.000000Z",
      "sponsor_logo": {
        "id": 85,
        "uuid": "714716f7-c562-47b4-856b-67657228f817",
        "sponsor_id": 60,
        "sponsor_logo": "images\/SponsorLogo\/5fe8d948d4219_sponsor_logo_.png",
        "created_at": "2020-12-27T18:58:16.000000Z",
        "updated_at": "2020-12-27T18:58:17.000000Z"
      }
    },
    {
      "id": 61,
      "uuid": "d8f42324-59c1-4ce1-88bc-705c3c906da3",
      "user_id": null,
      "event_id": 963,
      "sponsor_name": "Sponsor -media",
      "representative_name": "UKa",
      "representative_email": "uka@mailinator.com",
      "about_sponsor": "lldksfmalsdkfasd",
      "sponsorship_type": "media partner",
      "sponsorship_type_other": null,
      "sponsor_level": "gold",
      "app_visibility_level": 0,
      "created_at": "2020-12-27T18:59:02.000000Z",
      "created_by": 171,
      "updated_at": "2020-12-27T18:59:02.000000Z",
      "sponsor_logo": {
        "id": 86,
        "uuid": "3f72aa42-3c47-46d6-a3e1-40257d88fa6d",
        "sponsor_id": 61,
        "sponsor_logo": "images\/SponsorLogo\/5fe8d975ebd39_sponsor_logo_.png",
        "created_at": "2020-12-27T18:59:01.000000Z",
        "updated_at": "2020-12-27T18:59:02.000000Z"
      }
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
