import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
@Pipe({
  name: 'sponserFilter'
})
export class SponserFilterPipe implements PipeTransform {
  orders: any = ['gold', 'silver', 'bronze', 'basic'];

  transform(value: any, type): unknown {
    debugger
    value= value.filter(v=>v.sponsorship_type==type);
    return this.orderSponsor(value);
  }

  orderSponsor(value){
    let self = this;
      var sortedCollection = _.sortBy(value, function (item) {
        return self.orders.indexOf(item.sponsor_level)
      });
      return sortedCollection
  }
}
