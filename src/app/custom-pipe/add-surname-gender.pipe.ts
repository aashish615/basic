import { Pipe , PipeTransform} from '@angular/core';
import { Transform } from 'stream';
 
@Pipe({
    name: 'addSurname'
})

export class AddSurnamePipe implements PipeTransform{
    transform(value:any, arg:any){
         if(arg.toLowerCase() =="male"){
            return 'Mr'+value
        }else{
            return 'Mrs'+value
        }
     }
}
