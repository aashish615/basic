import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSurnamePipe } from './add-surname-gender.pipe';
import { SponserFilterPipe } from './sponser-filter.pipe';
 


@NgModule({
  declarations: [
    AddSurnamePipe,
    SponserFilterPipe,
    
  ],
  imports: [
    CommonModule
  ],
  exports:[
    AddSurnamePipe,
    SponserFilterPipe
  ]
})
export class CustomPipeModule { }
